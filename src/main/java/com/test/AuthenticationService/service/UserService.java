package com.test.AuthenticationService.service;

import com.test.AuthenticationService.entities.UserEntity;
import com.test.AuthenticationService.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;
    public UserEntity save(final UserEntity user) {
        return userRepository.save(user);
    }

    public UserEntity update(final UserEntity user) {
        if ( ! userRepository.existsById(user.getId())) {
            throw new IllegalArgumentException("No user found with the given id " + user.getUserId());
        }
        return userRepository.save(user);
    }

    public UserEntity findById(final String id) {
        return userRepository.findById(id).orElse(null);
    }

    public List<UserEntity> findAll() {
        final List<UserEntity> userList = new ArrayList<>();
        userRepository.findAll().forEach(userList::add);
        return userList;
    }

    public void deleteById(final String id) {
        userRepository.deleteById(id);
    }

    public void deleteAll() {
        userRepository.deleteAll();
    }
}
