package com.test.AuthenticationService.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.stereotype.Component;

import java.util.Date;

public class Entity {


    private Date createdOn;
    private Date updatedOn;
    private String createdBy;
    private String updatedBy;
}
