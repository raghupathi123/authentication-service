package com.test.AuthenticationService.repositories;

import com.test.AuthenticationService.entities.UserEntity;
import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.springframework.data.repository.CrudRepository;

@EnableScan
public interface UserRepository extends CrudRepository<UserEntity, String> {
}
